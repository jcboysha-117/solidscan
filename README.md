# SolidScan - Readme
Vulnerability scanner for fingerprint-based vulnerability scanning. Relies on 
network scanning. Not a solid system scanner; will need to build a cpe generator
for that... stay tuned. 

Scans a defined network for live hosts, scans the live hosts for open ports and
fingerprints the services on the ports, then it compares those to a CPE 
dictionary to convert from 2.2 to 2.3, finally, it compares the CVEs from that
list to the NVD JSON feeds.

## Included utilities
There are also additional utilities to the vulnerability scanning tool necessary
for generating the local feed repositories and cpe databases. These are used by
the setup and update processes, but can also be used independently.

## Installation
Clone the repository and make sure you have Masscan and nmap installed on your 
system. 

You can use the configuration schema below and examples provided in 
default-configuration.json and the conf.json that comes with the repo. 

### Prerequisites
#### Software
* nmap
* masscan

#### libraries
* python3-nmap
* python-masscan

## Usage instructions
running '''python3 autoscan.py -h''' will provide you with help. And show you 
the parameters you can configure. 

## Notes 
    __may some day become documentation!__

### Order of precedent for arguments
Command Line, Configuration file, Default

### Schema for Configuration file:
<pre>
{
    nmap-scripts-path: Path to nmap scripting engine folder,
    rate: default rate in kilopackets per second. This will only apply if the 
           scan configuration is not set.,
    ports: default port range to scan. Recommended set to 0-1024, although
            comma-separated lists will also work. This will
            only apply if the scan port range is not set.,
    output-directory: default directory to save output files. Will only apply
                        if the command line --outpath or -op are not set.,
    default-filename: default output file name. Will only apply if a filename
                       is not specified at runtime.,  
    scan-file: A separate json file with scans laid out as below. Not used by
                 default. Will only apply if a scan file is not specified at
                 the command line.,
    cpe-dictionary-file: A path to a file containing the NVD CPE Dictionary 
                         from https://nvd.nist.gov/products/cpe.,
    json-nvd-feeds: A path to a directory containing NVD Feed formatted CVE
                    feeds from https://nvd.nist.gov/vuln/data-feeds#JSON_FEED.,
    scans: [
        scan:{
            scope: ip scope of the scan in CIDR format,
            ports: scan configured port range, this will only apply if the 
                    command-line parameter --ports, -p is not set,
            rate: scan rate in kilopackets per second. This will only apply if 
            the command-line parameter --rate, -r is not set.
            scan-filename: name of the file to output the scan results to. 
                            Json format. REQUIRED FOR MULTIPLE SCANS.
            }
        ]
}
</pre>

### Schema for Scans files
<pre>
{
    scans: [
        scan:{
            scope: ip scope of the scan in CIDR format,
            ports: scan configured port range, this will only apply if the 
                    command-line parameter --ports, -p is not set,
            rate: scan rate in kilopackets per second. This will only apply if 
            the command-line parameter --rate, -r is not set.
            scan-filename: name of the file to output the scan results to. 
                            Json format. REQUIRED FOR MULTIPLE SCANS.
            }
        ]
}
</pre>

# License
Copyright (C) 2021 J.C. Boysha

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, Version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses/.
